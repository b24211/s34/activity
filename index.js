// Using require directive to load express module/package
const express = require("express");

// Create an application using express
const app = express();

// The port where the app will listen to
const port = 3000;

// Allows the server to handle data from request
// Allows the app to read json data
app.use(express.json());

// Allows the app to read data from forms
app.use(express.urlencoded({extended:true}));

// GET Route
app.get("/", (req, res) => {
	res.send("Hello world!");
})

// GET request at URI "/hello"
app.get("/hello", (req, res) => {
	res.send("Hello from the /hello endpoint!");
})

// POST Route
app.post("/hello", (req, res) => {
	res.send(`Hello there ${req.body.firstName} ${req.body.lastName}!`);
})

// An array that will store user objects when the "/signup" route is accessed
// This array serves as the mock database
let users = [];

// POST Request to register a user.
app.post("/signup", (req, res) => {
	console.log(req.body);

	if(req.body.username !== '' && req.body.password !== ''){
		// Stores the user object to the users array created
		users.push(req.body);

		res.send(`User ${req.body.username} successfully registered!`);
	}
	else{
		res.send("Please input BOTH username and password.");
	}

})

// PUT request to change the password of a specific user
app.put("/change-password", (req, res) => {
	// creates a variable to store the message to be sent back to the client/Postman
	let message;

	// creates a for loop that will iterate the elements of the "users" array
	for(let i = 0; i < users.length; i++){

		if(req.body.username == users[i].username){
			// Changes the password of the user found by the loop into the password provided in the client/postman
			users[i].password = req.body.password;

			message = `User ${req.body.username}'s password has been updated`;

			// terminates the loop
			break;
		}
		else{
			message = "User does not exist.";
		}
	}

	res.send(message);
})



// s34 Activity

app.get("/home", (req, res) => {
    res.send("Welcome to the home page");
})

app.get("/users", (req, res) => {
    res.send(users);
})

app.delete("/delete-user", (req, res) => {
 
    let message;
    if (users.length != 0){
        for(let i = 0; i < users.length; i++){
            if (req.body.username == users[i].username) { 
                users.splice(users[i], 1);
                message = `User ${req.body.username} has been deleted.`;
                break;
            } 
        }
    } else {
        message = "User does not exist.";
    }   
    res.send(message);
    
})


// Tells the server to listen to the port
app.listen(port, () => console.log(`Server running at port ${port}`));